﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Mover_Arriba = new System.Windows.Forms.Button();
            this.Mover_Abajo = new System.Windows.Forms.Button();
            this.Mover_Izquierda = new System.Windows.Forms.Button();
            this.Mover_Derecha = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Atacar_Arriba = new System.Windows.Forms.Button();
            this.Atacar_Izquierda = new System.Windows.Forms.Button();
            this.Atacar_Derecha = new System.Windows.Forms.Button();
            this.Atacar_Abajo = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pbjugador = new System.Windows.Forms.PictureBox();
            this.pbEspada = new System.Windows.Forms.PictureBox();
            this.pbMurcielago = new System.Windows.Forms.PictureBox();
            this.pbFantasma = new System.Windows.Forms.PictureBox();
            this.pbDemonio = new System.Windows.Forms.PictureBox();
            this.pbArco = new System.Windows.Forms.PictureBox();
            this.pbMazo = new System.Windows.Forms.PictureBox();
            this.pbPosionverde = new System.Windows.Forms.PictureBox();
            this.pbPosionBlanca = new System.Windows.Forms.PictureBox();
            this.pbArco2 = new System.Windows.Forms.PictureBox();
            this.pbposionverde2 = new System.Windows.Forms.PictureBox();
            this.pbPosionBlanca2 = new System.Windows.Forms.PictureBox();
            this.pbEspada2 = new System.Windows.Forms.PictureBox();
            this.pbMazo2 = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbjugador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEspada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMurcielago)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFantasma)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDemonio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMazo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPosionverde)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPosionBlanca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArco2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbposionverde2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPosionBlanca2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEspada2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMazo2)).BeginInit();
            this.SuspendLayout();
            // 
            // Mover_Arriba
            // 
            this.Mover_Arriba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Mover_Arriba.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mover_Arriba.Location = new System.Drawing.Point(755, 319);
            this.Mover_Arriba.Name = "Mover_Arriba";
            this.Mover_Arriba.Size = new System.Drawing.Size(87, 25);
            this.Mover_Arriba.TabIndex = 1;
            this.Mover_Arriba.Text = "Arriba";
            this.Mover_Arriba.UseVisualStyleBackColor = false;
            // 
            // Mover_Abajo
            // 
            this.Mover_Abajo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Mover_Abajo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mover_Abajo.Location = new System.Drawing.Point(755, 364);
            this.Mover_Abajo.Name = "Mover_Abajo";
            this.Mover_Abajo.Size = new System.Drawing.Size(87, 25);
            this.Mover_Abajo.TabIndex = 2;
            this.Mover_Abajo.Text = "Abajo";
            this.Mover_Abajo.UseVisualStyleBackColor = false;
            // 
            // Mover_Izquierda
            // 
            this.Mover_Izquierda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Mover_Izquierda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mover_Izquierda.Location = new System.Drawing.Point(671, 341);
            this.Mover_Izquierda.Name = "Mover_Izquierda";
            this.Mover_Izquierda.Size = new System.Drawing.Size(82, 26);
            this.Mover_Izquierda.TabIndex = 3;
            this.Mover_Izquierda.Text = "Izquierda";
            this.Mover_Izquierda.UseVisualStyleBackColor = false;
            // 
            // Mover_Derecha
            // 
            this.Mover_Derecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Mover_Derecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mover_Derecha.Location = new System.Drawing.Point(846, 341);
            this.Mover_Derecha.Name = "Mover_Derecha";
            this.Mover_Derecha.Size = new System.Drawing.Size(74, 26);
            this.Mover_Derecha.TabIndex = 4;
            this.Mover_Derecha.Text = "Derecha";
            this.Mover_Derecha.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(604, 296);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 26);
            this.label2.TabIndex = 10;
            this.label2.Text = "Atacar:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(604, 196);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Movimientos:";
            // 
            // Atacar_Arriba
            // 
            this.Atacar_Arriba.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Atacar_Arriba.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Atacar_Arriba.Location = new System.Drawing.Point(755, 211);
            this.Atacar_Arriba.Name = "Atacar_Arriba";
            this.Atacar_Arriba.Size = new System.Drawing.Size(87, 25);
            this.Atacar_Arriba.TabIndex = 6;
            this.Atacar_Arriba.Text = "Arriba";
            this.Atacar_Arriba.UseVisualStyleBackColor = false;
            // 
            // Atacar_Izquierda
            // 
            this.Atacar_Izquierda.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Atacar_Izquierda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Atacar_Izquierda.Location = new System.Drawing.Point(669, 238);
            this.Atacar_Izquierda.Name = "Atacar_Izquierda";
            this.Atacar_Izquierda.Size = new System.Drawing.Size(84, 26);
            this.Atacar_Izquierda.TabIndex = 9;
            this.Atacar_Izquierda.Text = "Izquierda";
            this.Atacar_Izquierda.UseVisualStyleBackColor = false;
            // 
            // Atacar_Derecha
            // 
            this.Atacar_Derecha.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Atacar_Derecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Atacar_Derecha.Location = new System.Drawing.Point(846, 238);
            this.Atacar_Derecha.Name = "Atacar_Derecha";
            this.Atacar_Derecha.Size = new System.Drawing.Size(74, 26);
            this.Atacar_Derecha.TabIndex = 8;
            this.Atacar_Derecha.Text = "Derecha";
            this.Atacar_Derecha.UseVisualStyleBackColor = false;
            // 
            // Atacar_Abajo
            // 
            this.Atacar_Abajo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.Atacar_Abajo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Atacar_Abajo.Location = new System.Drawing.Point(755, 263);
            this.Atacar_Abajo.Name = "Atacar_Abajo";
            this.Atacar_Abajo.Size = new System.Drawing.Size(87, 25);
            this.Atacar_Abajo.TabIndex = 7;
            this.Atacar_Abajo.Text = "Abajo";
            this.Atacar_Abajo.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(607, 50);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(235, 143);
            this.panel1.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(64, 113);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 15);
            this.label7.TabIndex = 4;
            this.label7.Text = "Demonio:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(64, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 15);
            this.label6.TabIndex = 3;
            this.label6.Text = "Fantasma:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(64, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Murcielago:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(64, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 15);
            this.label4.TabIndex = 1;
            this.label4.Text = "Jugador:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-3, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "Daño:";
            // 
            // pbjugador
            // 
            this.pbjugador.BackColor = System.Drawing.Color.Transparent;
            this.pbjugador.Image = ((System.Drawing.Image)(resources.GetObject("pbjugador.Image")));
            this.pbjugador.Location = new System.Drawing.Point(77, 211);
            this.pbjugador.Name = "pbjugador";
            this.pbjugador.Size = new System.Drawing.Size(31, 32);
            this.pbjugador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbjugador.TabIndex = 12;
            this.pbjugador.TabStop = false;
            // 
            // pbEspada
            // 
            this.pbEspada.BackColor = System.Drawing.Color.Transparent;
            this.pbEspada.Image = ((System.Drawing.Image)(resources.GetObject("pbEspada.Image")));
            this.pbEspada.Location = new System.Drawing.Point(114, 211);
            this.pbEspada.Name = "pbEspada";
            this.pbEspada.Size = new System.Drawing.Size(35, 31);
            this.pbEspada.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEspada.TabIndex = 13;
            this.pbEspada.TabStop = false;
            // 
            // pbMurcielago
            // 
            this.pbMurcielago.BackColor = System.Drawing.Color.Transparent;
            this.pbMurcielago.Image = ((System.Drawing.Image)(resources.GetObject("pbMurcielago.Image")));
            this.pbMurcielago.Location = new System.Drawing.Point(155, 211);
            this.pbMurcielago.Name = "pbMurcielago";
            this.pbMurcielago.Size = new System.Drawing.Size(37, 32);
            this.pbMurcielago.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMurcielago.TabIndex = 14;
            this.pbMurcielago.TabStop = false;
            // 
            // pbFantasma
            // 
            this.pbFantasma.BackColor = System.Drawing.Color.Transparent;
            this.pbFantasma.Image = ((System.Drawing.Image)(resources.GetObject("pbFantasma.Image")));
            this.pbFantasma.Location = new System.Drawing.Point(198, 211);
            this.pbFantasma.Name = "pbFantasma";
            this.pbFantasma.Size = new System.Drawing.Size(29, 32);
            this.pbFantasma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFantasma.TabIndex = 15;
            this.pbFantasma.TabStop = false;
            // 
            // pbDemonio
            // 
            this.pbDemonio.BackColor = System.Drawing.Color.Transparent;
            this.pbDemonio.Image = ((System.Drawing.Image)(resources.GetObject("pbDemonio.Image")));
            this.pbDemonio.Location = new System.Drawing.Point(233, 211);
            this.pbDemonio.Name = "pbDemonio";
            this.pbDemonio.Size = new System.Drawing.Size(41, 32);
            this.pbDemonio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbDemonio.TabIndex = 16;
            this.pbDemonio.TabStop = false;
            // 
            // pbArco
            // 
            this.pbArco.BackColor = System.Drawing.Color.Transparent;
            this.pbArco.Image = ((System.Drawing.Image)(resources.GetObject("pbArco.Image")));
            this.pbArco.Location = new System.Drawing.Point(280, 211);
            this.pbArco.Name = "pbArco";
            this.pbArco.Size = new System.Drawing.Size(38, 32);
            this.pbArco.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbArco.TabIndex = 18;
            this.pbArco.TabStop = false;
            // 
            // pbMazo
            // 
            this.pbMazo.BackColor = System.Drawing.Color.Transparent;
            this.pbMazo.Image = ((System.Drawing.Image)(resources.GetObject("pbMazo.Image")));
            this.pbMazo.Location = new System.Drawing.Point(324, 211);
            this.pbMazo.Name = "pbMazo";
            this.pbMazo.Size = new System.Drawing.Size(33, 32);
            this.pbMazo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMazo.TabIndex = 19;
            this.pbMazo.TabStop = false;
            // 
            // pbPosionverde
            // 
            this.pbPosionverde.BackColor = System.Drawing.Color.Transparent;
            this.pbPosionverde.Image = ((System.Drawing.Image)(resources.GetObject("pbPosionverde.Image")));
            this.pbPosionverde.Location = new System.Drawing.Point(363, 211);
            this.pbPosionverde.Name = "pbPosionverde";
            this.pbPosionverde.Size = new System.Drawing.Size(28, 31);
            this.pbPosionverde.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPosionverde.TabIndex = 20;
            this.pbPosionverde.TabStop = false;
            // 
            // pbPosionBlanca
            // 
            this.pbPosionBlanca.BackColor = System.Drawing.Color.Transparent;
            this.pbPosionBlanca.Image = ((System.Drawing.Image)(resources.GetObject("pbPosionBlanca.Image")));
            this.pbPosionBlanca.Location = new System.Drawing.Point(397, 211);
            this.pbPosionBlanca.Name = "pbPosionBlanca";
            this.pbPosionBlanca.Size = new System.Drawing.Size(29, 31);
            this.pbPosionBlanca.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPosionBlanca.TabIndex = 21;
            this.pbPosionBlanca.TabStop = false;
            // 
            // pbArco2
            // 
            this.pbArco2.BackColor = System.Drawing.Color.Transparent;
            this.pbArco2.Image = ((System.Drawing.Image)(resources.GetObject("pbArco2.Image")));
            this.pbArco2.Location = new System.Drawing.Point(224, 319);
            this.pbArco2.Name = "pbArco2";
            this.pbArco2.Size = new System.Drawing.Size(60, 48);
            this.pbArco2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbArco2.TabIndex = 22;
            this.pbArco2.TabStop = false;
            // 
            // pbposionverde2
            // 
            this.pbposionverde2.BackColor = System.Drawing.Color.Transparent;
            this.pbposionverde2.Image = ((System.Drawing.Image)(resources.GetObject("pbposionverde2.Image")));
            this.pbposionverde2.Location = new System.Drawing.Point(155, 319);
            this.pbposionverde2.Name = "pbposionverde2";
            this.pbposionverde2.Size = new System.Drawing.Size(63, 48);
            this.pbposionverde2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbposionverde2.TabIndex = 23;
            this.pbposionverde2.TabStop = false;
            // 
            // pbPosionBlanca2
            // 
            this.pbPosionBlanca2.BackColor = System.Drawing.Color.Transparent;
            this.pbPosionBlanca2.Image = ((System.Drawing.Image)(resources.GetObject("pbPosionBlanca2.Image")));
            this.pbPosionBlanca2.Location = new System.Drawing.Point(290, 319);
            this.pbPosionBlanca2.Name = "pbPosionBlanca2";
            this.pbPosionBlanca2.Size = new System.Drawing.Size(62, 48);
            this.pbPosionBlanca2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPosionBlanca2.TabIndex = 24;
            this.pbPosionBlanca2.TabStop = false;
            // 
            // pbEspada2
            // 
            this.pbEspada2.BackColor = System.Drawing.Color.Transparent;
            this.pbEspada2.Image = ((System.Drawing.Image)(resources.GetObject("pbEspada2.Image")));
            this.pbEspada2.Location = new System.Drawing.Point(86, 319);
            this.pbEspada2.Name = "pbEspada2";
            this.pbEspada2.Size = new System.Drawing.Size(63, 48);
            this.pbEspada2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEspada2.TabIndex = 25;
            this.pbEspada2.TabStop = false;
            // 
            // pbMazo2
            // 
            this.pbMazo2.BackColor = System.Drawing.Color.Transparent;
            this.pbMazo2.Image = ((System.Drawing.Image)(resources.GetObject("pbMazo2.Image")));
            this.pbMazo2.Location = new System.Drawing.Point(363, 319);
            this.pbMazo2.Name = "pbMazo2";
            this.pbMazo2.Size = new System.Drawing.Size(63, 48);
            this.pbMazo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMazo2.TabIndex = 26;
            this.pbMazo2.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(686, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 47);
            this.label8.TabIndex = 27;
            this.label8.Text = "The Quest";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(846, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 32);
            this.button1.TabIndex = 28;
            this.button1.Text = "Reiniciar";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button2.Location = new System.Drawing.Point(847, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(105, 32);
            this.button2.TabIndex = 29;
            this.button2.Text = "Salir";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button3.Location = new System.Drawing.Point(847, 126);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(105, 32);
            this.button3.TabIndex = 30;
            this.button3.Text = "Ayuda";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.Location = new System.Drawing.Point(846, 161);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(106, 32);
            this.button4.TabIndex = 31;
            this.button4.Text = "Estadisticas";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(956, 402);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pbMazo2);
            this.Controls.Add(this.pbEspada2);
            this.Controls.Add(this.pbPosionBlanca2);
            this.Controls.Add(this.pbposionverde2);
            this.Controls.Add(this.pbArco2);
            this.Controls.Add(this.pbPosionBlanca);
            this.Controls.Add(this.pbPosionverde);
            this.Controls.Add(this.pbMazo);
            this.Controls.Add(this.pbArco);
            this.Controls.Add(this.pbDemonio);
            this.Controls.Add(this.pbFantasma);
            this.Controls.Add(this.pbMurcielago);
            this.Controls.Add(this.pbEspada);
            this.Controls.Add(this.pbjugador);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Mover_Abajo);
            this.Controls.Add(this.Atacar_Izquierda);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Mover_Derecha);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Mover_Izquierda);
            this.Controls.Add(this.Mover_Arriba);
            this.Controls.Add(this.Atacar_Abajo);
            this.Controls.Add(this.Atacar_Derecha);
            this.Controls.Add(this.Atacar_Arriba);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbjugador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEspada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMurcielago)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFantasma)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDemonio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMazo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPosionverde)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPosionBlanca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbArco2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbposionverde2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPosionBlanca2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEspada2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMazo2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Mover_Arriba;
        private System.Windows.Forms.Button Mover_Abajo;
        private System.Windows.Forms.Button Mover_Izquierda;
        private System.Windows.Forms.Button Mover_Derecha;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Atacar_Arriba;
        private System.Windows.Forms.Button Atacar_Izquierda;
        private System.Windows.Forms.Button Atacar_Derecha;
        private System.Windows.Forms.Button Atacar_Abajo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pbjugador;
        private System.Windows.Forms.PictureBox pbEspada;
        private System.Windows.Forms.PictureBox pbMurcielago;
        private System.Windows.Forms.PictureBox pbFantasma;
        private System.Windows.Forms.PictureBox pbDemonio;
        private System.Windows.Forms.PictureBox pbArco;
        private System.Windows.Forms.PictureBox pbMazo;
        private System.Windows.Forms.PictureBox pbPosionverde;
        private System.Windows.Forms.PictureBox pbPosionBlanca;
        private System.Windows.Forms.PictureBox pbArco2;
        private System.Windows.Forms.PictureBox pbposionverde2;
        private System.Windows.Forms.PictureBox pbPosionBlanca2;
        private System.Windows.Forms.PictureBox pbEspada2;
        private System.Windows.Forms.PictureBox pbMazo2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}

