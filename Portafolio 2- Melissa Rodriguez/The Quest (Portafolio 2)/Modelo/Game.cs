﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace WindowsFormsApplication1.Modelo
{
    #region "Enums"
    public enum Direcciones
    {
        Arriba,
        Derecha,
        Abajo,
        Izquierda
    }
    #endregion

    class Game
    {
        public List<Enemigo> Enemigos;

        #region "Campos"
        private int nivel = 0;
        public Arma armaEquipada;
        public Jugador jugador;
        private Rectangle limites;
        #endregion

        #region "Propiedades"
        public List<string> ArmasJugador
        {
            get { return jugador.Armas; }
        }

        public int Nivel
        {
            get { return nivel; }
        }

        public Point PosicionJugador
        {
            get { return jugador.Localizacion; }
        }

        public int VidaJugador
        {
            get { return jugador.Vida; }
        }

        public Rectangle Limites
        {
            get { return Limites; }
        }
        #endregion

        #region "Constructores"
        public Game()
        {
        }

        public Game(Rectangle limites)
        {
            this.limites = limites;
            jugador = new Jugador(this, new Point(limites.Left + 10, limites.Top + 70));
        }
        #endregion

        #region "Metodos/Funciones"
        public void Mover(Direcciones direcciones, Random random)
        {
            jugador.Mover(direcciones);
            foreach (Enemigo enemigo in Enemigos)
                enemigo.Move(random);
        }

        public void Equipar(string nombreArma)
        {
            jugador.Equipar(nombreArma);
        }

        public void DanioJugador(int maxDanio, Random random)
        {
            jugador.GolpearmaxDanio(maxDanio, random);
        }

        public void AumentarVida(int vida, Random random)
        {
            jugador.IncrementarVida(vida, random);
        }

        public void Atacar(Direcciones direcciones, Random random)
        {
            jugador.Atacar(direcciones, random);

            foreach (Enemigo enemigo in Enemigos)
                enemigo.Move(random);
        }

        public bool Buscarposicion(string nombrePosion)
        {
            return jugador.BuscarPosionUsada(nombrePosion);
        }

        public bool BuscarInventario(string nombreArma)
        {
            return jugador.Armas.Contains(nombreArma);
        }

        public Point ObtenerPosicion(Random random)
        {
            return new Point(limites.Left + random.Next(limites.Right / 10 - limites.Left / 10) * 10,
                limites.Top + random.Next(limites.Bottom / 10 - limites.Top / 10) * 10);
        }

        public void NivelNuevo(Random random)
        {
            nivel++;
            
            switch (nivel)
            {
                case 1:
                    Enemigos = new List<Enemigo>();
                    Enemigos.Add(new Murcielago(this, ObtenerPosicion(random)));
                    armaEquipada = new Espada(this, ObtenerPosicion(random));
                    break;
                
                case 2:
                    Enemigos = new List<Enemigo>();
                    Enemigos.Add(new Fantasma(this, ObtenerPosicion(random)));
                    armaEquipada = new PosionBlanca(this, ObtenerPosicion(random));
                    break;
                
                case 3:
                    Enemigos = new List<Enemigo>();
                    Enemigos.Add(new Demonio(this, ObtenerPosicion(random)));
                    armaEquipada = new Arco(this, ObtenerPosicion(random));
                    break;
                
                case 4:
                    Enemigos = new List<Enemigo>();
                    Enemigos.Add(new Murcielago(this, ObtenerPosicion(random)));
                    Enemigos.Add(new Fantasma(this, ObtenerPosicion(random)));

                    if (BuscarInventario("Arco"))
                    {
                        if (!BuscarInventario("Posion Blanca") || (BuscarInventario("Posion Blanca") && BuscarInventario("Posion Blanca")))
                        {
                            armaEquipada = new PosionBlanca(this, ObtenerPosicion(random));
                        }
                        else
                        {
                            armaEquipada = new Arco(this, ObtenerPosicion(random));
                        }
                    }
                    break;

                case 5:
                    Enemigos = new List<Enemigo>();
                    Enemigos.Add(new Murcielago(this, ObtenerPosicion(random)));
                    Enemigos.Add(new Demonio(this, ObtenerPosicion(random)));
                    armaEquipada = new PosionVerde(this, ObtenerPosicion(random));
                    break;

                case 6:
                    Enemigos = new List<Enemigo>();
                    Enemigos.Add(new Fantasma(this, ObtenerPosicion(random)));
                    Enemigos.Add(new Demonio(this, ObtenerPosicion(random)));
                    armaEquipada = new Mazo(this, ObtenerPosicion(random));
                    break;
                
                case 7:
                    Enemigos = new List<Enemigo>();
                    Enemigos.Add(new Murcielago(this, ObtenerPosicion(random)));
                    Enemigos.Add(new Fantasma(this, ObtenerPosicion(random)));
                    Enemigos.Add(new Demonio(this, ObtenerPosicion(random)));
                   
                    if (BuscarInventario("Mazo"))
                    {
                        if (!BuscarInventario("Posion Verde") || (BuscarInventario("Posion Verde") && BuscarInventario("Posion Verde")))
                        {
                            armaEquipada = new PosionVerde(this, ObtenerPosicion(random));
                        }
                        else
                        {
                            armaEquipada = new Mazo(this, ObtenerPosicion(random));
                        }
                    }
                    break;
                
                case 8:
                    armaEquipada = null;
                    break;
                
                case 9:
                    Environment.Exit(1);
                    break;
            }
        }
        #endregion


        abstract class Personaje
        {
            #region "Campos"
            private const int MoverIntervalo = 10;
            protected Point localizacion;
            protected Game game;
            protected Point pLocalizacion;
            #endregion

            #region "propiedades"
            public Point Localizacion
            {
                get
                {
                    return localizacion;
                }
            }
            #endregion

            #region "Constructores"
            public Personaje()
            {
            }
            public Personaje(Game game, Point localizacion)
            {
                this.game = game;
                this.localizacion = localizacion;
            }
            #endregion

            #region "Metodos/Funciones"
            public bool Cercano(Point buscarPosicion, int distancia)
            {
                if (Math.Abs(localizacion.X - buscarPosicion.X) < distancia && (Math.Abs(localizacion.Y - buscarPosicion.Y) < distancia))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Funcion para mover con los botones del teclado al jugador
            /// </summary>
            /// <param name="direcciones"></param>
            /// <param name="limites"></param>
            /// <returns></returns>
            public Point Mover(Direcciones direcciones, Rectangle limites)
            {
                Point nLocalizacion = localizacion;
                switch (direcciones)
                {
                    case Direcciones.Arriba:
                        if (nLocalizacion.Y - MoverIntervalo >= limites.Top)
                            nLocalizacion.Y -= MoverIntervalo;
                        break;
                    case Direcciones.Abajo:
                        if (nLocalizacion.Y + MoverIntervalo <= limites.Bottom)
                            nLocalizacion.Y += MoverIntervalo;
                        break;
                    case Direcciones.Izquierda:
                        if (nLocalizacion.X - MoverIntervalo >= limites.Left)
                            nLocalizacion.X -= MoverIntervalo;
                        break;
                    case Direcciones.Derecha:
                        if (nLocalizacion.X + MoverIntervalo <= limites.Right)
                            nLocalizacion.X += MoverIntervalo;
                        break;
                    default: break;
                }
                return nLocalizacion;

            }
            #endregion
        }

        public class Jugador : Personaje
        {
            private List<Arma> Inventario = new List<Arma>();

            #region "Campos"
            private int vida;
            private Arma armaEquipada;
            #endregion

            #region "Propiedades"
            public int Vida
            {
                get { return vida; }
            }

            public List<string> Armas
            {
                get
                {
                    List<string> nombres = new List<string>();

                    foreach (Arma arma in Inventario)
                        nombres.Add(arma.Nombre);
                    return nombres;
                }
            }
            #endregion

            #region "Constructores"
            public Jugador()
            {
            }

            public Jugador(Game game,
                                        Point localizacion)
                : base(game, localizacion)
            {
                vida = 10;
            }
            #endregion

            #region "Metodos/Funciones"
            public bool BuscarPosionUsada(string nombrePosion)
            {
                IPosion posion;
                bool posionUsada = true;

                foreach (Arma arma in Inventario)
                {
                    if (arma.Nombre == nombrePosion && arma is IPosion)
                    {
                        posion = arma as IPosion;
                        posionUsada = posion.Usado;
                    }
                }
                return posionUsada;
            }
            public void GolpearmaxDanio(int maxDanio, Random random)
            {
                vida -= random.Next(1, maxDanio);
            }
            public void IncrementarVida(int vida, Random random)
            {
                this.vida += random.Next(1, vida);
            }
            public void Equipar(string nombreArma)
            {
                foreach (Arma arma in Inventario)
                {
                    if (arma.Nombre == nombreArma)
                        armaEquipada = arma;
                }
            }
            public void Mover(Direcciones direcciones)
            {
                base.localizacion = Mover(direcciones, game.Limites);
                if (!game.armaEquipada.Seleccionada)
                {
                    if (Cercano(game.armaEquipada.Localizacion, 1))
                    {
                        game.armaEquipada.SeleccionarArma();
                        Inventario.Add(game.armaEquipada);

                    }
                }


            }
            public void Atacar(Direcciones direcciones, Random random)
            {
                if (armaEquipada != null)
                {
                    armaEquipada.Atacar(direcciones, random);
                }
            }
            #endregion
        }

        abstract class Arma : Personaje
        {
            #region "Campos"
            protected Game game;
            private bool seleccionada;
            private Point localizacion;
            private const int MoverIntervalo = 10;
            #endregion

            #region "Propiedades"
            public bool Seleccionada
            {
                get { return seleccionada; }
            }

            public Point Localizacion
            {
                get { return localizacion; }
            }

            public abstract string Nombre
            {
                get;
            }

            #endregion

            #region "Constructores"
            public Arma()
            {

            }
            public Arma(Game game,
                                    Point localizacion)
            {
                this.game = game;
                this.localizacion = localizacion;
                seleccionada = false;
            }
            #endregion

            #region "Metodos/Funciones"
            public Point Mover(Direcciones direcciones, Point localizacion, Rectangle limites)
            {
                Point nLocalizacion = pLocalizacion;
                switch (direcciones)
                {
                    case Direcciones.Arriba:
                        if (localizacion.Y - MoverIntervalo >= limites.Top)
                            localizacion.Y -= MoverIntervalo;
                        break;
                    case Direcciones.Abajo:
                        if (localizacion.Y + MoverIntervalo <= limites.Bottom)
                            localizacion.Y += MoverIntervalo;
                        break;
                    case Direcciones.Izquierda:
                        if (localizacion.X - MoverIntervalo >= limites.Left)
                            localizacion.X -= MoverIntervalo;
                        break;
                    case Direcciones.Derecha:
                        if (localizacion.X + MoverIntervalo <= limites.Right)
                            localizacion.X += MoverIntervalo;
                        break;
                    default: break;
                }
                return localizacion;
            }
            public bool Cercano(Point verLocalizacion, Point blanco, int radio)
            {
                if (Math.Abs(blanco.X - verLocalizacion.X) < radio && (Math.Abs(blanco.Y - verLocalizacion.Y) < radio))
                    return true;
                else
                    return false;
            }
            public void SeleccionarArma()
            {
                seleccionada = true;
            }
            public abstract void Atacar(Direcciones direcciones, Random random);
            protected bool DanioEnemigo(Direcciones direcciones, int radio, int danio, Random random)
            {
                Point blanco = game.PosicionJugador;
                for (int distancia = 0; distancia < radio; distancia++)
                {
                    foreach (Enemigo enemigo in game.Enemigos)
                    {
                        if (Cercano(enemigo.Localizacion, blanco, radio))
                        {
                            enemigo.Golpear(danio, random);
                            return true;
                        }
                    }
                    blanco = Mover(direcciones, blanco, game.Limites);
                }
                return false;
            }
            protected Direcciones AtaqueSabio(Direcciones direccion)
            {
                Direcciones Sabio = direccion;
                if (direccion == Direcciones.Arriba)
                    Sabio = Direcciones.Derecha;
                if (direccion == Direcciones.Derecha)
                    Sabio = Direcciones.Abajo;
                if (direccion == Direcciones.Abajo)
                    Sabio = Direcciones.Izquierda;
                if (direccion == Direcciones.Izquierda)
                    Sabio = Direcciones.Arriba;
                return Sabio;
            }
            protected Direcciones AtaqueCounterSabio(Direcciones direcciones)
            {
                Direcciones CounterSabio = direcciones;
                if (direcciones == Direcciones.Arriba)
                    CounterSabio = Direcciones.Izquierda;
                if (direcciones == Direcciones.Derecha)
                    CounterSabio = Direcciones.Arriba;
                if (direcciones == Direcciones.Abajo)
                    CounterSabio = Direcciones.Derecha;
                if (direcciones == Direcciones.Izquierda)
                    CounterSabio = Direcciones.Abajo;
                return CounterSabio;

            }
            #endregion
        }

        class Espada : Arma
        {
            #region "Propiedades"
            public override string Nombre
            {
                get { return "Espada"; }
            }
            #endregion

            #region "Constructores"
            public Espada()
            {
            }

            public Espada(Game game,
                                    Point localizacion)
                : base(game, localizacion)
            {

            }
            #endregion

            #region "Metodos/Funciones"

            public override void Atacar(Direcciones direcciones, Random random)
            {
                Direcciones DireccionAtaque;
                if (!DanioEnemigo(direcciones, 10, 3, random))
                {
                    DireccionAtaque = AtaqueSabio(direcciones);
                    if (!DanioEnemigo(DireccionAtaque, 10, 3, random))
                    {
                        DireccionAtaque = AtaqueCounterSabio(direcciones);
                        DanioEnemigo(DireccionAtaque, 10, 3, random);
                    }
                }

            }

            #endregion
        }

        class Mazo : Arma
        {
            #region "Propiedades"
            public override string Nombre
            {
                get { return "Mazo"; }
            }
            #endregion

            #region "Constructores"
            public Mazo(Game game,
                                    Point localizacion)
                : base(game, localizacion)
            {
            }
            #endregion


            #region "Métodos/Funciones"
            public override void Atacar(Direcciones direcciones, Random random)
            {
                if (!DanioEnemigo(direcciones, 20, 6, random))
                {
                    Direcciones proximaDireccion = AtaqueSabio(direcciones);

                    for (int i = 0; i <= 2; i++)
                    {
                        if (DanioEnemigo(proximaDireccion, 20, 6, random))
                        {
                            break;
                        }
                        proximaDireccion = AtaqueSabio(direcciones);
                    }
                }
            }

            #endregion
        }

        class Arco : Arma
        {
            #region "Propiedades"
            public override string Nombre
            {
                get { return "Arco"; }
            }
            #endregion

            #region "Constructores"
            public Arco(Game game,
                                    Point localizacion)
                : base(game, localizacion)
            {
            }
            #endregion


            #region "Metodos/Funciones"
            public override void Atacar(Direcciones direcciones, Random random)
            {
                DanioEnemigo(direcciones, 30, 1, random);
            }
            #endregion
        }

        class PosionBlanca : Arma, IPosion
        {
            #region "Propiedades"
            public override string Nombre
            {
                get { return "Posion Azul"; }
            }

            public bool Usado
            {
                get;
                private set;
            }
            #endregion

            #region "Constructores"
            public PosionBlanca(Game game,
                                        Point localizacion)
                : base(game, localizacion)
            {
                Usado = false;
            }
            #endregion


            #region "Metodos/Funciones"
            public override void Atacar(Direcciones direcciones, Random random)
            {
                game.AumentarVida(10, random);
                Usado = true;
            }
            #endregion
        }

        class PosionVerde : Arma, IPosion
        {
            #region "Propiedades"
            public override string Nombre
            {
                get { return "Posion Roja"; }
            }

            public bool Usado
            {
                get;
                private set;
            }
            #endregion

            #region "Constructores"
            public PosionVerde(Game game,
                                        Point localizacion)
                : base(game, localizacion)
            {
                Usado = false;
            }
            #endregion


            #region "Metodos/Funciones"
            public override void Atacar(Direcciones direcciones, Random random)
            {
                game.AumentarVida(5, random);
                Usado = true;
            }
            #endregion
        }

        public abstract class Enemigo : Personaje
        {
            #region "Campos"
            private const int CercaDistanciaJugador = 25;
            private int danios;
            #endregion

            #region "Propiedades"
            public int Danios
            {
                get { return danios; }
            }

            public bool Muerto
            {
                get
                {
                    if (danios <= 0)
                        return true;

                    else
                        return false;
                }

            }
            #endregion

            #region "Constructores"
            public Enemigo()
            {

            }
            public Enemigo(Game game,
                                    Point localizacion,
                                    int danios)
                : base(game, localizacion)
            {
                this.danios = danios;
            }
            #endregion


            #region "Metodos/Funciones"
            public abstract void Move(Random random);
            public void Golpear(int maxDanio, Random random)
            {
                danios -= random.Next(1, maxDanio);
            }
            protected bool CercaJugador()
            {
                return (Cercano(game.PosicionJugador, CercaDistanciaJugador));
            }
            protected Direcciones EncontrarDireccionJugador(Point posicionJugador)
            {
                Direcciones moverseDireccion;

                if (posicionJugador.X > localizacion.X + 10)
                {
                    moverseDireccion = Direcciones.Derecha;
                }
                else if (posicionJugador.X < localizacion.X - 10)
                {
                    moverseDireccion = Direcciones.Izquierda;
                }
                else if (posicionJugador.Y < localizacion.Y - 10)
                {
                    moverseDireccion = Direcciones.Arriba;
                }
                else
                {
                    moverseDireccion = Direcciones.Abajo;
                }
                return moverseDireccion;
            }
            #endregion
        }

        class Fantasma : Enemigo
        {
            #region "Constructores"
            public Fantasma(Game game,
                                        Point localizacion)
                : base(game, localizacion, 8)
            {

            }
            #endregion


            #region "Metodos/Funciones"
            public override void Move(Random random)
            {
                if (!Muerto)
                {
                    if (random.Next(1, 3) == 1 || random.Next(1, 3) == 2)
                    {
                        localizacion = Mover(EncontrarDireccionJugador(game.PosicionJugador), game.Limites);
                    }
                    if (CercaJugador())
                    {
                        game.DanioJugador(3, random);
                    }
                }
            }
            #endregion
        }

        class Murcielago : Enemigo
        {
            #region "Propiedades"
            public Murcielago()
            {
            }

            public Murcielago(Game game,
                                        Point localizacion)
                : base(game, localizacion, 6)
            {

            }
            #endregion


            #region "Metodos/Funciones"

            public override void Move(Random random)
            {
                if (!Muerto)
                {
                    if (random.Next(0, 2) == 1)
                    {
                        localizacion = Mover(EncontrarDireccionJugador(game.PosicionJugador), game.Limites);

                    }
                    else
                    {
                        localizacion = Mover((Direcciones)random.Next(1, 4), game.Limites);
                    }
                    if (CercaJugador())
                    {
                        game.DanioJugador(2, random);
                    }
                }
            }

            #endregion
        }

        class Demonio : Enemigo
        {
            #region "Constructores"
            public Demonio(Game game,
                                    Point localizacion)
                : base(game, localizacion, 10)
            {

            }
            #endregion


            #region "Metodos/Funciones"
            public override void Move(Random random)
            {
                if (!Muerto)
                {
                    if (random.Next(0, 2) == 1)
                    {
                        localizacion = Mover(EncontrarDireccionJugador(game.PosicionJugador), game.Limites);

                    }
                    else
                    {
                        localizacion = Mover((Direcciones)random.Next(1, 4), game.Limites);
                    }
                    if (CercaJugador())
                    {
                        game.DanioJugador(2, random);
                    }
                }
            }
            #endregion
        }

        public interface IPosion
        {
            #region "Propiedades"
            bool Usado { get; }
            #endregion
        }
    }
}
    

