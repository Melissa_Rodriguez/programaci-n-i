﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1.Modelo;
using System.Threading;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private Game game;
        private Random random = new Random();
        public Form1()
        {
            InitializeComponent();
        }
        public void ActualizarPersonajes()
        {
            int enemigosMostrados = 0;
            pbjugador.Location = game.PosicionJugador;
            lVidaJugador.Text = game.VidaJugador.ToString();
            pbEspada.Visible = false;
            pbMurcielago.Visible = false;
            pbDemonio.Visible = false;
            pbFantasma.Visible = false;
            pbArco.Visible = false;
            pbPosionverde.Visible = false;
            pbPosionBlanca.Visible = false;
            pbMazo.Visible = false;
            

            foreach (Enemigo enemigo in game.Enemigos)
            {
                if (enemigo is Murcielago)
                {
                    lMurcielago.Location = enemigo.Localizacion;
                    lVidaMurcielago.Text = enemigo.Danios.ToString();

                    if (enemigo.Danios > 0)
                    {
                        enemigosMostrados++;
                        lMurcielago.Visible = true;
                    }
                    else
                    {
                        lMurcielago.Visible = false;
                    }
                }
                if (enemigo is Fantasma)
                {
                    lFantasma.Location = enemigo.Localizacion;
                    lVidaFantasma.Text = enemigo.Danios.ToString();
                    if (enemigo.Danios > 0)
                    {
                        lFantasma.Visible = true;
                        enemigosMostrados++;
                        lVidaFantasma.Visible = true;
                    }
                    else
                    {
                        lVidaFantasma.Visible = false;

                    }
                }

                if (enemigo is CosaRara)
                {
                    lCosaRara.Location = enemigo.Localizacion;
                    lVidaDemonio.Text = enemigo.Danios.ToString();
                    if (enemigo.Danios > 0)
                    {
                        lCosaRara.Visible = true;
                        enemigosMostrados++;
                        lVidaDemonio.Visible = true;
                    }
                    else
                    {
                        lVidaDemonio.Visible = false;
                    }

                }
            }

            lEspada.Visible = false;
            lPosionAzul.Visible = false;
            lArco.Visible = false;
            lPosionRoja.Visible = false;
            lMaso.Visible = false;

            if (juego.ArmaEquipada != null)
            {
                Control armaControl = null;
                switch (juego.ArmaEquipada.Nombre)
                {
                    case "Espada":
                        armaControl = lEspada;
                        break;

                    case "Posion Roja":
                        armaControl = lPosionRoja;
                        break;

                    case "Posion Azul":
                        armaControl = lPosionAzul;
                        break;
                    case "Arco":
                        armaControl = lArco;
                        break;

                    case "Mazo":
                        armaControl = lMaso;
                        break;
                }

                if (juego.ArmaEquipada.Seleccionada)
                {
                    armaControl.Visible = false;
                }
                else
                {
                    armaControl.Visible = true;
                    armaControl.Location = juego.ArmaEquipada.Localizacion;
                }
            }
            lEspada1.Visible = false;
            lPosionAzul1.Visible = false;
            lArco1.Visible = false;
            lPosionRoja1.Visible = false;
            lMaso1.Visible = false;

            if (juego.ChequearInventario("Espada"))
            {
                lEspada1.Visible = true;
            }

            if (juego.ChequearInventario("Posion Azul"))
            {
                if (!juego.ChequearPosion("Posion Azul"))
                {
                    lPosionAzul1.Visible = true;
                }
            }

            if (juego.ChequearInventario("Arco"))
            {
                lArco1.Visible = true;
            }
            if (juego.ChequearInventario("Posion Roja"))
            {
                if (!juego.ChequearPosion("Posion Roja"))
                {
                    lPosionRoja1.Visible = true;
                }
            }

            if (juego.ChequearInventario("Mazo"))
            {
                lMaso1.Visible = true;
            }
            if (juego.VidaJugador <= 0)
            {
                MessageBox.Show("Haz Muerto!");
                Application.Restart();
            }
            if (enemigosMostrados == 0)
            {
                MessageBox.Show("Haz derrotado a tus enemigos.");
                if (juego.Nivel != 8)
                {
                    juego.NuevoNivel(random);
                    ActualizarPersonajes();
                }
                if (juego.Nivel == 8)
                {
                    lJugador.Visible = false;
                    MessageBox.Show("Felicidades!!");
                    Thread.Sleep(500);
                    juego.NuevoNivel(random);
                }
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {
            
        }
       
    }
}
