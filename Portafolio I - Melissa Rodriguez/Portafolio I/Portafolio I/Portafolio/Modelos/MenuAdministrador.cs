﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Portafolio.Modelos
{
    public partial class MenuAdministrador : Form
    {
        public MenuAdministrador()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 forma2 = new Form2();
            forma2.Visible = true;
            this.Hide(); 
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form1 frm = new Form1();
            frm.Visible = true;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DatosEmpleados frmDatos = new DatosEmpleados();
            frmDatos.Visible = true;
            this.Hide(); 
        }

        private void MenuAdministrador_Load(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            DatosEmpleados fmr = new DatosEmpleados();
            fmr.Visible = true;
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CrearAsignatura fmr = new CrearAsignatura();
            fmr.Visible = true;
            this.Hide();
        }
    }
}
