﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Portafolio.Modelos
{
   public class Asignatura
    {
        #region "Campos"
        string creditos;
        string nombreAsig;
        string profAsig;
        string codigoAsig;
        #endregion

        #region "Propiedades"
        public string Creditos
        {
            get { return creditos; }
            set { creditos = value; }
        }

        public string NombreAsignatura
        {
            get { return nombreAsig; }
            set { nombreAsig = value; }
        }

        public string ProfesorAsignado
        {
            get { return profAsig; }
            set { profAsig = value; }
        }

        public string CodigoAsignatura
        {
            get { return codigoAsig; }
            set { codigoAsig = value; }
        }
        #endregion

        #region "Constructores"
        public Asignatura()
        { 
        
        }

        public Asignatura(string Creditos,
                                        string NombreAsignatura,
                                        string ProfesorAsignado,
                                        string DiasAsignados,
                                        string CodigoAsignatura)
        {
            this.NombreAsignatura = NombreAsignatura;
            this.ProfesorAsignado = ProfesorAsignado;
            this.Creditos = Creditos;
            this.CodigoAsignatura = CodigoAsignatura;
        }
        #endregion
    }
}
