﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Portafolio.Modelos
{
    public partial class DatosEmpleados : Form
    {
        Administrativo Admin = new Administrativo();
        
        public DatosEmpleados()
        {
            Admin.ListaProfesores = new List<Profesor>();
            Admin.ListaEmpleados = new List<Administrativo>();
            InitializeComponent();
        }

        private void DatosEmpleados_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MenuAdministrador frmAdm = new MenuAdministrador();
            frmAdm.Visible = true;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           Admin.ListaProfesores.Add(new Profesor(textbox1.Text, 
                                                                    textBox2.Text, 
                                                                    textBox3.Text, 
                                                                    textBox4.Text, 
                                                                    textBox5.Text, 
                                                                    textBox6.Text, 
                                                                    textBox7.Text, 
                                                                    textBox8.Text, 
                                                                    textBox9.Text));
            textbox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Admin.ListaProfesores.Add(new Profesor(textbox1.Text,
                                                                    textBox2.Text,
                                                                    textBox3.Text,
                                                                    textBox4.Text,
                                                                    textBox5.Text,
                                                                    textBox6.Text,
                                                                    textBox7.Text,
                                                                    textBox8.Text,
                                                                    textBox9.Text));
            textbox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
        }
    }
}
