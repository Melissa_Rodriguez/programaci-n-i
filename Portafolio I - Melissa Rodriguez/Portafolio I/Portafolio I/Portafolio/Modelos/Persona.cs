﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Portafolio;

namespace Portafolio.Modelos
{
    public class Persona
    {
        #region "Campos"
        readonly int id;
        static int _ultimoId = 0;
        string nombre;
        string apellido;
        string nacionalidad;
        string cedula;
        string direccion;
        string telefono;
        string email;
        string tandaDisponible;
        #endregion

        #region "Propiedades"

        public int Id
        {
            get { return id; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public string Nacionalidad
        {
            get { return nacionalidad; }
            set { nacionalidad = value; }
        }

        public string Cedula
        {
            get { return cedula; }
            set { cedula = value; }
        }

        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public string TandaDisponible 
        {
            get { return tandaDisponible; }
            set { tandaDisponible = value; }
        }
        #endregion

        #region "Constructores"
        public Persona()
        {
            this.id = _ultimoId++;
        }

        public Persona(string Nombre,
                                     string Apellido,
                                     string Nacionalidad,
                                     string Cedula,
                                     string Direccion,
                                     string Telefono,
                                     string Email,
                                     string TandaDisponible) : this ()
        {
            this.Nombre = Nombre;
            this.Apellido = Apellido;
            this.Nacionalidad = Nacionalidad;
            this.Cedula = Cedula;
            this.Direccion = Direccion;
            this.Telefono = Telefono;
            this.Email = Email;
            this.TandaDisponible = TandaDisponible;
        }
        #endregion
    }
    
    public class Estudiante : Persona
    {
        #region "Campos"
        string matricula;
        string carrera;
        DateTime _horaLlegada;
        Boolean _llego;
        #endregion

        #region "Propiedades"  
        public string Matricula
        {
            get { return matricula; }
            set { matricula = value; }
        }

        public string Carrera
        {
            get { return carrera; }
            set { carrera = value; }
        }

        public DateTime HoraLlegada
        {
            get { return _horaLlegada; }
            set { _horaLlegada = value; }
        }

        public Boolean Llego
        {
            get { return _llego; }
            set { _llego = value; }
        }
        #endregion

        #region "Constructores"
        public Estudiante() 
        {
        }

        public Estudiante(string Nombre,
                                        string Apellido,
                                        string Nacionalidad,
                                        string Cedula,
                                        string Direccion,
                                        string Telefono,
                                        string Email,
                                        string Matricula, 
                                        string Carrera,
                                        string TandaDisponible) : base(Nombre, 
                                                                        Apellido,
                                                                        Nacionalidad,
                                                                        Cedula,
                                                                        Direccion,
                                                                        Telefono,
                                                                        Email,
                                                                        TandaDisponible)                                                                        
        {
            this.Matricula = Matricula;
            this.Carrera = Carrera;
            this.HoraLlegada = HoraLlegada;
            this. Llego = Llego;
        }
        #endregion

        #region "Metodos/Funciones" 
        public void Llegar()
        {
            this.Llego = true;
            this.HoraLlegada = DateTime.Now;
        }
        #endregion
     }

   public class Profesor : Persona
    {
        #region "Campos"
        string codigo;
        #endregion

        #region "Propiedades"
        public string Codigo
        {
                get { return codigo; }
                set { codigo = value; }
        }
        #endregion

        #region "Constructores"
        public Profesor()
        {
        }

        public Profesor (string Nombre,
                                       string Apellido,
                                       string Nacionalidad,
                                       string Cedula,
                                       string Direccion,
                                       string Telefono,
                                       string Email,
                                       string Codigo,
                                       string TandaDisponible) : base (Nombre,
                                                                               Apellido,
                                                                               Nacionalidad,
                                                                               Cedula,
                                                                               Direccion,
                                                                               Telefono,
                                                                               Email,
                                                                               TandaDisponible) 
        {
            this.Codigo = Codigo;
        }
        #endregion
    }

    public class Administrativo : Persona
    {

        #region "Campos"
        string codigo_Empleado;
        public List<Estudiante> ListaEstudiante;
        public List<Profesor> ListaProfesores;
        public List<Administrativo> ListaEmpleados;
        public List<Asignatura> ListaAsignatura;
        #endregion

        #region "Propiedades"
        public string CodigoEmpleado
        {
            get { return codigo_Empleado;}
		    set { codigo_Empleado = value;}

        }
        #endregion

        #region "Constructores"
        public Administrativo()
        {
        }

        public Administrativo(string Nombre,
                                        string Apellido,
                                        string Nacionalidad,
                                        string Cedula,
                                        string Direccion,
                                        string Telefono,
                                        string Email,
                                        int codigoEmpleado,
                                        string TandaDisponible) : base (Nombre,
                                                                         Apellido,
                                                                         Nacionalidad,
                                                                         Cedula,
                                                                         Direccion,
                                                                         Telefono,
                                                                         Email,
                                                                         TandaDisponible) 
        {
            this.CodigoEmpleado = CodigoEmpleado;
        }
        #endregion
    }  
}

