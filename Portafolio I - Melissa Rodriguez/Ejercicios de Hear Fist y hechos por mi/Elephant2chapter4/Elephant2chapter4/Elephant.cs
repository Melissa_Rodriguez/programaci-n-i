﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Elephant2chapter4
{
    class Elephant
    {
        public String Name;
        public int EarSize;

        public void TellMe(string message, Elephant whoSaidIt) 
        {
            MessageBox.Show(whoSaidIt.Name + " Says... " + message,this.Name);
        
        
        }

        public void SpeakToMe(Elephant whoToTalk, string message) 
        {
            whoToTalk.TellMe(message, this);
        
        }
    }


}
