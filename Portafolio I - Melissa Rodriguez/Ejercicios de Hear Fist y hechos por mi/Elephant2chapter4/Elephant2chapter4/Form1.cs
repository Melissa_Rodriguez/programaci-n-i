﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Elephant2chapter4
{
    public partial class Form1 : Form
    {
        Elephant Lloyd;
        Elephant Lucinda;
        public Form1()
        {  
            InitializeComponent();
            Lloyd = new Elephant { Name = "Lloyd", EarSize = 32};
            Lucinda = new Elephant { Name = "Lucinda", EarSize = 44};

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Lloyd.TellMe("Hi", Lucinda);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Lucinda.SpeakToMe(Lloyd, "Como estas?");
        }
        
    }
}
