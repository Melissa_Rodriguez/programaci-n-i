﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MenuMakerChapter4
{
    class MenuMaker
    {
        public Random Ramdomize;
        public string[] Meats = {"Roast beef", "Salami", "Turkey", "Ham", "Pastrami"};
        public string[] Breads = {"rye","white","wheat","pumpernickel","italian bread", "a roll"};
        public string[] Contiments = {"yellow mustard", "brown mustard", "honey mustard", "mayo", "relish", "french dressing"};

        public string GetMenuItem() 
        {
            string ramdomMeat = Meats[Ramdomize.Next(Meats.Length)];
            string ramdomBreads = Breads[Ramdomize.Next(Breads.Length)];
            string ramdomContiments = Contiments[Ramdomize.Next(Contiments.Length)];

            return ramdomMeat + " With " + ramdomContiments + " on " + ramdomBreads;
                   
        }

    }
}
