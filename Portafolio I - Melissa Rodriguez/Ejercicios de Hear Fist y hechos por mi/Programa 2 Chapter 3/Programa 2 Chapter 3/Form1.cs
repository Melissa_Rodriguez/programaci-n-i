﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Programa_2_Chapter_3
{
    public partial class Form1 : Form
    {
        Guy joe;
        Guy bob;
        int bank = 100;
        public void UpdateForm()
        {
            joesCashLabel.Text = joe.Name + " has $ " + joe.Cash;
            bobsCashLabel.Text = bob.Name + " has $ " + bob.Cash;
            bankCashLabel.Text = "The Bank has $" + bank;

        }
        public Form1()
        {
            InitializeComponent();
            joe = new Guy();
            bob = new Guy();
            joe.Name = "Joe";
            bob.Name = "Bob";
            joe.Cash = 100;
            bob.Cash = 50;
            UpdateForm();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (bank >= 10)
            {
                bank -= joe.RecieveCash(10);
                UpdateForm();

            }
            else 
            {
                MessageBox.Show("The banks is out of money...");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bank += bob.GiveCash(5);
            UpdateForm();
        }

        
    }
}
