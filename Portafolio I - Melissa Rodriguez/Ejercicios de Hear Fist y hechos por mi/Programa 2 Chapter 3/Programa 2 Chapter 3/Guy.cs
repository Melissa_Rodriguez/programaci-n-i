﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace Programa_2_Chapter_3
{
    class Guy
    {
        public string Name;
        public int Cash;

        public int GiveCash(int amount) 
        {
            if (amount > 0 && amount <= Cash)
            {
                Cash -= amount;
                return amount;
            }
            else 
            {
                MessageBox.Show("I don't have enough Cash to give you $" + amount + " "+ Name + " Say...");
                return 0;
            }
        }
        public int RecieveCash(int amount) 
        {
            if (amount > 0)
            {
                Cash += amount;
                return amount;
            }
            else 
            {
                MessageBox.Show(amount + "isn't a cash i would take..." + Name + " Says..." );
                return 0;
            }
        }
   }
}
