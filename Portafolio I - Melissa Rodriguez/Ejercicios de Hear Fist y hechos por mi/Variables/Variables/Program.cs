﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Variables     // se usan para guardar un tipo de dato que luego puede ser usado en otros programas sin necesidad de aber lo que tiene dentro
{
    class Program
    {
        static void Main(string[] args)
        {
            int numeroEntero = 4;
            String cadena = "Melissa Rodriguez";
            float numeroFlotante = 2.5f;
            double numeroFlotante2 = 24.2432454355345;
            char caracter = 'a';
            long numeroGrande = 2434534543344345;
            short numeroChico = 349;
            decimal numeroDecimal = 121;
            
            Console.WriteLine("Mi nombre es " +cadena);   //imprimir en pantalla el valor de una variable
            Console.ReadLine();
        }
    }
}
