﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Elephant_chapter_4
{
    public partial class Form1 : Form
    {
        Elephant lucinda = new Elephant() { Name = "Lucinda" , EarSize =33 };
        Elephant Lloyd = new Elephant() { Name = "Lloyd" , EarSize =40 };
        
        



        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lucinda.WhoIam();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Lloyd.WhoIam();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Elephant Pedro;
            Pedro = lucinda;
            lucinda = Lloyd;
            Lloyd = Pedro;
            MessageBox.Show("Objects Swapped");

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Lloyd = lucinda;
            Lloyd.EarSize = 4321;
            Lloyd.WhoIam();

        }

       

        
    }
}
