﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace mileage_Calculator
{
    public partial class Form1 : Form
    {
        int startingMileage;
        int endingMileage;
        double mileageTraveled;
        double reimburseRate = .39;
        double amountOwed;
        
        
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            startingMileage = (int)numericUpDown1.Value;
            endingMileage = (int)numericUpDown2.Value;
            if (startingMileage > endingMileage)
            {
                MessageBox.Show("The starting mileage must be less than the ending mileage", "Cannot be Calcule");
            
            }
            else 
            {
             mileageTraveled = endingMileage-startingMileage;
             amountOwed = mileageTraveled * reimburseRate;
             label4.Text="$" + amountOwed;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show(mileageTraveled + " miles", "miles traveled");
        }

       
    }
}
